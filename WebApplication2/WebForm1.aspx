﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="left">

            <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="False" DataSourceID="XmlDataSource1" Width="471px" DataKeyNames="ID" OnRowCommand="Grid1_RowCommand" OnSelectedIndexChanged="Grid1_SelectedIndexChanged">
                <SelectedRowStyle CssClass="GridViewSelectedRow" />
                <HeaderStyle CssClass="GridViewHeader" />
                <RowStyle CssClass="GridViewRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Title:">
                        <ItemTemplate>
                            <%# XPath("Title") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />


                    <asp:CommandField ShowSelectButton="true" SelectText="View details" />
                </Columns>
            </asp:GridView>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/DVD.xml"></asp:XmlDataSource>

            <br />
            <br />

        </div>
                <div id="right">
                        <asp:Label CssClass="Header" runat="server">Details</asp:Label>
        <asp:DetailsView ID="details" runat="server" DataSourceID="XmlDataSource2" AutoGenerateRows="false" GridLines="None">
            <FieldHeaderStyle CssClass="DetailViewFieldHeader" />
            <RowStyle CssClass="DetailViewRow" />
            <Fields>
                <asp:TemplateField HeaderText="ID:">
                    <ItemTemplate>
                        <%# XPath("@ID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title:">
                    <ItemTemplate>
                        <%# XPath("Title") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Director">
                    <ItemTemplate>
                        <%# XPath("Director") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Price:">
                    <ItemTemplate>
                        <%# XPath("Price") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Stars:">
                    <ItemTemplate>
                        <asp:DataList ID="DataList1" runat="server" DataSource='<%# XPathSelect("Starring/Star") %>'>
                            <ItemTemplate>
                                <%# XPath(".") %>
                                <br />
                            </ItemTemplate>

                        </asp:DataList>
                    </ItemTemplate>
                </asp:TemplateField>

            </Fields>
        </asp:DetailsView>
        <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/DVD.xml"></asp:XmlDataSource>
        <br />
                    </div>
    </form>
</body>
</html>
