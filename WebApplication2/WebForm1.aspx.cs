﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Grid1.SelectedIndex = 0;
        }
        protected void Grid1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                Grid1.SelectedIndex = index;
            }
        }

        protected void Grid1_SelectedIndexChanged(object sender, EventArgs e)
        {
            XmlDataSource2.XPath = "/DvdList/DVD[@ID=" + Grid1.SelectedDataKey.Value.ToString() + "]";
        }

    }
}